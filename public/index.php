<?php

// Path to your craft/ folder
$craftPath = '../craft';
define('CRAFT_CONFIG_PATH', '../config/');
define('CRAFT_PLUGINS_PATH', '../plugins/');
define('CRAFT_STORAGE_PATH', '../storage/');
define('CRAFT_TEMPLATES_PATH', '../templates/');

// Do not edit below this line
$path = rtrim($craftPath, '/').'/app/index.php';

if (!is_file($path))
{
	if (function_exists('http_response_code'))
	{
		http_response_code(503);
	}

	exit('Could not find your craft/ folder. Please ensure that <strong><code>$craftPath</code></strong> is set correctly in '.__FILE__);
}

require_once $path;
