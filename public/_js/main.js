$(window).on('scroll resize', function(){
	var scroll = $(window).scrollTop();
	var size = $(window).width();
	if ( $('body').hasClass('homepage') )
	{
		if ( size < 768 )
		{
			if (scroll >= 96 && !$('#nav-main').hasClass('show')) {
				$('[role=banner]').addClass('fixed');
			}
			
			if (scroll < 96 && $('[role=banner]').hasClass('fixed') )
			{
				$('[role=banner]').removeClass('fixed');
			}
		}
		if ( size >= 768 )
		{
			if (scroll >= 96) {
				$('[role=banner]').addClass('fixed');
			}
			if (scroll < 96 && $('[role=banner]').hasClass('fixed') )
			{
				$('[role=banner]').removeClass('fixed');
			}
		}
	}
})
$(window).on('load resize', function(){
	var size = $(window).width();
	
	if ( $('body').hasClass('page-case-study') ) 
	{
		if ( size >= 768 && !$('#project-overview').hasClass('background-applied') )
		{
			var $image = $('#project-overview').data('background'),
				$retinaImage = $('#project-overview').data('backgroundRetina'),
				$background = '<div class="background"><img srcset="' + $image +' 1x, ' + $retinaImage +' 2x" alt="" /></div>';
				$('#project-overview').append($background).addClass('background-applied');
				
		}
		
		if ( size >= 768 && !$('#project-hero').hasClass('background-applied') )
		{
			var $backgroundImage = $('#project-hero').data('background');
			$('#project-hero').css({
				'backgroundImage' : 'url("' + $backgroundImage + '")',
				'backgroundColor' : 'transparent'
				}).addClass('background-applied');
				
		}
		if ( size >= 768 && !$('.longvids').hasClass('background-applied') )
		{
			var $backgroundImage = $('.longvids').data('background');
			$('.longvids').css({
				'backgroundImage' : 'url("' + $backgroundImage + '")',
				'backgroundColor' : 'transparent'
				}).addClass('background-applied');
				
		}
	}
})
function getVideoModal(v) {
	 $videoModal = '<div class="modal"><div class="modal-logo"></div><div class="close-btn close">Close<span></span><span></span></div><div class="video-modal"><div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;"><div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;"><div class="wistia_embed wistia_async_'+v+' videoFoam=true" style="height:100%;width:100%">&nbsp;</div></div></div></div></div>';
	return $videoModal;
}

function getVimeoVideo(vimeo) {
	var $vimeoVideoModal = '<div class="modal"><div class="modal-logo"></div><div class="close-btn close">Close<span></span><span></span></div><div class="video-modal"><div style="padding:56.25% 0 0 0;position:relative;"><div style="height:100%;left:0;position:absolute;top:0;width:100%;"><iframe src="https://player.vimeo.com/video/'+vimeo+'?autoplay=1" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen style="height:100%;width:100%"></iframe></div></div></div></div>';
	return $vimeoVideoModal;
}
if ($('.video-open'.length)) {
	$('.video-open').click(function(e) {
		e.preventDefault();
		var id = $(this).attr('data-video');
		var id_sub = id.substring(0,3);
		var vimeo = $(this).attr('data-vimeo');
		
		if (vimeo != undefined)
		{
			var modal = getVimeoVideo(vimeo);
		}
		else {
			var modal = getVideoModal(id);
		}
		
		$('body').append('<div class="veil fade in"></div>'+modal);
		$('.modal').fadeIn('fast', function() {
			$('.video-modal',this).delay(500).fadeTo(500,1,function() {
				$('.modal .close').show();
			});
		});
		window._wq = window._wq || [];
		_wq.push({ "_all": function(video) {
			video.play();
			video.bind("end", function(t) {
				$('.modal .close').hide();
				$('.modal .video-modal').fadeTo(500,0,function() {
					$('.modal').hide();
					$('.veil,.modal').remove();
				});
			});
		}});
		$('.modal .close').bind('click', function() {
			$('.modal .close').hide();
			$('.modal .video-modal').fadeTo(500,0,function() {
				$('.modal').hide();
				$('.veil,.modal').remove();
			});
		});
	});
}

if (window.location.hash === "#reel")
{
	$(window).load(function() {
		
		var id = $('#watch-our-reel').attr('data-video');
		var id_sub = id.substring(0,3);
		var vimeo = $(this).attr('data-vimeo');
		
		if (vimeo != undefined)
		{
			var modal = getVimeoVideo(vimeo);
		}
		else {
			var modal = getVideoModal(id);
		}
		
		$('body').append('<div class="veil fade in"></div>'+modal);
		$('.modal').fadeIn('fast', function() {
			$('.video-modal',this).delay(500).fadeTo(500,1,function() {
				$('.modal .close').show();
			});
		});
		window._wq = window._wq || [];
		_wq.push({ "_all": function(video) {
			video.play();
			video.bind("end", function(t) {
				$('.modal .close').hide();
				$('.modal .video-modal').fadeTo(500,0,function() {
					$('.modal').hide();
					$('.veil,.modal').remove();
				});
			});
		}});
		$('.modal .close').bind('click', function() {
			$('.modal .close').hide();
			$('.modal .video-modal').fadeTo(500,0,function() {
				$('.modal').hide();
				$('.veil,.modal').remove();
			});
		});
	});
}
if ( $('body').hasClass('page-video-production') )
{	
	if ( window.location.hash ) {
		var $videoID = window.location.hash;
		
		if ( $(''+$videoID+'').length )
		{
			
			$(window).load(function() {
				var id = $(''+$videoID+'').attr('data-video');
				var id_sub = id.substring(0,3);
				var vimeo = $(''+$videoID+'').attr('data-vimeo');
		
				if (vimeo != undefined)
				{
					var modal = getVimeoVideo(vimeo);
				}
				else {
					var modal = getVideoModal(id);
				}
				
				$('body').append('<div class="veil fade in"></div>'+modal);
				$('.modal').fadeIn('fast', function() {
					$('.video-modal',this).delay(500).fadeTo(500,1,function() {
						$('.modal .close').show();
					});
				});
				window._wq = window._wq || [];
				_wq.push({ "_all": function(video) {
					video.play();
					video.bind("end", function(t) {
						$('.modal .close').hide();
						$('.modal .video-modal').fadeTo(500,0,function() {
							$('.modal').hide();
							$('.veil,.modal').remove();
						});
					});
				}});
				$('.modal .close').bind('click', function() {
					$('.modal .close').hide();
					$('.modal .video-modal').fadeTo(500,0,function() {
						$('.modal').hide();
						$('.veil,.modal').remove();
					});
				});
			});
		}
	}
}


if ($('.team-listing').length) {
	$('#team-nav a').click(function(event) {
		event.preventDefault();
		$('#team-nav a').each(function() {
			$(this).removeClass('active');
		});
		$(this).addClass('active');
		var href = $(this).attr('href');
		
		var hash = href.substring(1);

		if (hash == 'all') {
			$('figure').fadeOut(200);
			$('figure').fadeIn(600);
		}
		else {
			$('figure').fadeOut(200);
			$('.'+hash).fadeIn(600);
			
		}
	});
}

function videoFilter ( $, window ) {
	window.watchResize = function( callback )
	{
		var resizing;
		function done()
		{
			clearTimeout( resizing );
			resizing = null;
			callback();
		}
		$(window).resize(function(){
			if ( resizing )
			{
				clearTimeout( resizing );
				resizing = null;
			}
			resizing = setTimeout( done, 50 );
		});
		// init
		callback();
	};
	window.watchResize(function() {
		var size = $(window).width();
		
		if ( size >= 768 )
		{
			if ( !$('#video-nav').hasClass('fired') )
			{
				$('#video-nav').addClass('fired');
				$('.video-container').fadeOut(200);
				$('.featured').fadeIn(600);
			}
			$('#video-nav a').click(function(event) {
				event.preventDefault();
				$('#video-nav a').each(function() {
					$(this).removeClass('active');
				});
				$(this).addClass('active');
				var href = $(this).attr('href');
				
				var hash = href.substring(1);

				if (hash == 'all') {
					$('.video-container').fadeOut(200);
					$('.video-container').fadeIn(600);
				}
				else {
					$('.video-container').fadeOut(200);
					$('.'+hash).fadeIn(600);
				}
			});
		}
		if ( size < 768 && $('#video-nav').addClass('fired') )
		{
			$('#video-nav').removeClass('fired');
		}
	});	
}
if ( $('body').hasClass('page-video-production') )
{
	videoFilter( jQuery, window );
}
function dboyResize ( $, window ) {

	window.watchResize = function( callback )
	{
		var resizing;
		function done()
		{
			clearTimeout( resizing );
			resizing = null;
			callback();
		}
		$(window).resize(function(){
			if ( resizing )
			{
				clearTimeout( resizing );
				resizing = null;
			}
			resizing = setTimeout( done, 50 );
		});
		// init
		callback();
	};
	window.watchResize(function(){
		var size = $(window).width();

		if ( size < 768 && !$('#nav-main').hasClass('mobile')) {
			$('#nav-main').addClass('mobile');
			$('#nav-button').unbind();
			$('#nav-main').removeClass('show');
			$('#nav-button').removeClass('open');
			$('#nav-button').on('click',function(e) {
				e.preventDefault();
				$(this).toggleClass('open');
				$('#nav-main').fadeToggle(1000, "linear").toggleClass('show');
				$('#next-section').toggleClass('hide');
			});
		}

		else if ( size >= 768 )
		{
			if ( $('#nav-main').hasClass('mobile') ) {
				$('#nav-main').removeClass('mobile');
			}
			if ( !$('#nav-main').hasClass('large-screen') )
			{
				$('#nav-main').addClass('large-screen');
				$('#nav-button').unbind();
				$('#nav-main').removeClass('show');
				$('#nav-button').removeClass('open');
				$('#nav-button').on('click',function(e) {
					e.preventDefault();
					$(this).toggleClass('open');
					$('#nav-main').toggleClass('show');
					$('#next-section').toggleClass('hide');
				});
			}
		}
	});


}
dboyResize( jQuery, window );

function projectsAnimation() {
	var size = $(window).width();
	var projectsTimer;
	var projectsDom = $("#projects ol").clone();
	var projectsNav = '<ol id="projects-nav"><li class="active"><a href="#silverleaf">Silverleaf</a><li><a href="#huntington">Huntington</a></li><li><a href="#axsium">Axsium</a></li></ol>';
	function slideshow() {
		//$('#our-work .wrapper').append(projectsNav);
		jQuery.fn.timer = function() {
				$( "#projects li:first-child" ).fadeOut('slow', function() {
					$( "#projects li:first-child" ).appendTo("#projects ol").delay(500).fadeIn('slow');
				});


				if (!$('#projects-nav').children('li:last-child').hasClass('active'))
				{
					$('#projects-nav').children('li.active')
						.removeClass('active')
						.next('li').addClass('active');
				}
				else
				{
					$('#projects-nav').children('li.active')
						.removeClass('active')
						.end().children("li:first")
						.addClass('active');
				}

		}

		projectsTimer = setInterval(function() {
			$("#projects ol, #projects-nav").timer();
		}, 5000);
	}

	function openingAnimation() {
		$('#projects ol li:first-child').css('top','142px');
		$('#projects ol li:nth-child(3)').css('top','160px');
		$('#projects').fadeIn('2500', function(){
			$('#projects ol li:first-child').animate({
				top:0,
			},1000, 'linear', function(){
				$('#projects ol li:first-child').css('top','');
			});
			$('#projects ol li:nth-child(3)').animate({
				top:300,
			},1000, 'linear', function(){
				$('#projects ol li:nth-child(3)').css('top','');
			});
		});

	}

	// This will execute on load if the browser window is less than 768 wide
	if ( size < 768 )
	{
		$('#projects ol').slick({
			slide:'#projects li',
			slidesToShow:1,
			slidestoScroll:1,
			arrows:false,
			dots:false,
			fade: true,
			pauseOnHover:false,
			cssEase: 'linear',
			autoplay:true,
			autoplaySpeed:5000,
		});
		$('#projects').addClass('slick-implemented');
	}

	// This will execute on load if the browser window is greater than 768 wide
	else if ( size >= 768 )
	{
		$('#projects').addClass('projects-slideshow');
		slideshow();
	}


	$(window).on('scroll resize', function(){
		var size = $(window).width();
		var scroll = $(this).scrollTop();
		var projectScroll = $('#our-work').offset().top - 72;

		if ( size < 768 )
		{
			if ( $('#projects').hasClass('projects-slideshow') )
			{
				window.clearInterval(projectsTimer);
				$('#projects').html(projectsDom);
				$('#projects-nav').remove();
				$('#projects').removeClass('projects-slideshow').removeClass('show');
			}
			if ( !$('#projects').hasClass('slick-implemented') )
			{
				$('#projects ol').slick({
					slide:'#projects li',
					slidesToShow:1,
					slidestoScroll:1,
					arrows:false,
					dots:false,
					fade: true,
					pauseOnHover:false,
					cssEase: 'linear',
					autoplay:true,
					autoplaySpeed:3000,
				});
				$('#projects').addClass('slick-implemented');
			}
		}

		// Prepares desktop animation or executes if resizing from smaller screen view
		else if ( size >= 768 && !$('#projects').hasClass('projects-slideshow') )
		{
			$('#projects').addClass('projects-slideshow');

			if ( $('#projects').hasClass('slick-implemented') )
			{
				$('#projects ol').slick('unslick');
				$('#projects').html(projectsDom);
				$('#projects').removeClass('slick-implemented');
				$('#projects').addClass('show');
			//	openingAnimation();
				slideshow();
			}
			else {
				slideshow();
			}

		}


		// Triggers the fadeIn and execution of desktop project animation
/*		if ( scroll >= projectScroll && size >= 768)
		{
			if ( !$('#projects').hasClass('show') )
			{
				$('#projects').addClass('show');
			//	openingAnimation();
				slideshow();
			}
		} */
	});
}
if ( $('body').hasClass('homepage') )
{
	projectsAnimation();

	$('#team-slideshow ol').slick({
		slide:'#team-slideshow li',
		infinite:true,
		slidesToShow:5,
		slidestoScroll:1,
		arrows: false,
		dots:false,
		autoplay:true,
		autoplaySpeed:5000,
		swipe:false,
		mobileFirst: true,
		responsive: [
			{
				breakpoint: 960,
				settings: {
					slidesToShow:7,
				}
			}
		]
	});

}

(function(){

  var parallax = document.querySelectorAll(".parallax"),
      speed = 0.5;

  window.onscroll = function(){
    [].slice.call(parallax).forEach(function(el,i){

      var windowYOffset = window.pageYOffset,
          elBackgrounPos = "50% " + (windowYOffset * speed) + "px";

      el.style.backgroundPosition = elBackgrounPos;

    });
  };

})();


function homeSlideshow() {

	var showTimer = setInterval(function() {
		$("#home-slideshow ol").showtimer();
	}, 5000);

	$('#home-slideshow li:first').addClass('active');
	jQuery.fn.showtimer = function() {
		if(!$(this).children('li:last-child').hasClass('active')){
			$(this).children('li.active')
				.removeClass('active')
				.next('li').addClass('active');
		}
		else{
			$(this).children('li.active')
				.removeClass('active')
			.end().children("li:first")
				.addClass('active');
		}
	}
}

if ( $('body').hasClass('homepage') )
{
	homeSlideshow();
}

$('#contact-form').submit(function(ev) {
    // Prevent the form from actually submitting
    ev.preventDefault();
   
	$('#contact-form').addClass('submit-show');
    // Get the post data
    var data = $(this).serialize();

    // Send it to the server
    $.post('/', data, function(response) {
        if (response.success) {
	        $('#contact-form').removeClass('submit-show');
	        $('.errors, .error-message,.error-header').remove();
	        $('#contact-form ol').fadeOut('fast');
            $('#thanks').fadeIn('slow');
            $('html, body').stop().animate({
				'scrollTop': $('#contact-content').offset().top-72
			}, 900, 'swing');
        }
        else

        {
	        // Remove previous error messages
	        $('#contact-form').removeClass('submit-show');
	        $('.error-message,.error-header').remove();
			$('#contact-form li').removeClass('error');
	        for ( var fieldId in response.error ) {

		            $('#'+fieldId).parent().addClass('error');
		        }
		    $('#contact-form').prepend('<h2 class="error-header">Oops!</h2><div class="error-message">Looks like you forgot something.<br/>Please fill out all the info below. Thanks. :)</div>');
	        }
    });
});

function smoothScroll() {
	var size = $(window).width();
	$(window).on('scroll resize', function(){
		var size = $(window).width();
		$('#next-section').on('click',function(e) {
			e.preventDefault();
			var target = this.hash;
			$target = $(target);
			if ( size >= 768 ) {
				$('html, body').stop().animate({
					'scrollTop': $target.offset().top
				}, 900, 'swing');
			}
			else {
				$('html, body').stop().animate({
					'scrollTop': $target.offset().top-72
				}, 900, 'swing');
	
			}
		});
		$('a[data-target="#free-templates-2"]').on('click',function(e) {
			e.preventDefault();
			if ( size >= 768 ) {
				$('html, body').stop().animate({
					'scrollTop': $('#free-templates-2').offset().top-148
				}, 900, 'swing');
			}
			else {
				$('html, body').stop().animate({
					'scrollTop': $('#free-templates-2').offset().top-128
				}, 900, 'swing');
	
			}
		});
	});
	$('#back-to-top').on('click',function(e) {
		e.preventDefault();
		var target = this.hash;
		$target = $(target);

		$('html, body').stop().animate({
			'scrollTop': $target.offset().top
		}, 900, 'swing');
	});
	$('#next-section').on('click',function(e) {
		e.preventDefault();
		var target = this.hash;
		$target = $(target);
		if ( size >= 768 ) {
			$('html, body').stop().animate({
				'scrollTop': $target.offset().top
			}, 900, 'swing');
		}
		else {
			$('html, body').stop().animate({
				'scrollTop': $target.offset().top-72
			}, 900, 'swing');

		}
	});
/*	$('#nav-main .nav-contact a, #nav-contact a').on('click',function(e) {
		e.preventDefault();
		var target = this.hash;
		$target = $(target);
		
		if ( $('#nav-main.mobile').hasClass('show') )
		{
			$('#nav-button').toggleClass('open');
			$('#nav-main').fadeToggle("fast", "linear").toggleClass('show');
		}
		
		if ( $('body').hasClass('recruitment-page') && size < 768 ) 
		{
			$('#nav-secondary').slideToggle();
		}

		$('html, body').stop().animate({
			'scrollTop': $target.offset().top-72
		}, 900, 'swing');
	});
	$('#nav-secondary .nav-contact a').on('click',function(e) {
		e.preventDefault();
		var target = this.hash;
		$target = $(target);
		
		$('#nav-secondary').slideToggle();
		$('html, body').stop().animate({
			'scrollTop': $target.offset().top-72
		}, 900, 'swing');
	}); */
}
smoothScroll();


(function($,window) {
	var $win = $(window),
	size = $win.width();

    var justExecuted = false;

    $(window).on('scroll', function() {
        // Slow down event execution with timeout set below
        if ( justExecuted ) {
        return;
        }

        // Cache all values and elements used below
        var width,
        $body = $(window),
        $scrollPosition = $body.scrollTop();

        // Switch became increasingly efficient here
        switch( true ) {
            case ($scrollPosition > 300):
                $('.back-to-top').addClass('active');
            break;

            case ($scrollPosition < 299):
                $('.back-to-top').removeClass('active');
            break;
        }

        // Reset timeout periodically (increase for better performance, but decreased accuracy)
        justExecuted = true;
        setTimeout(function() {
            justExecuted = false;
        }, 100);
    });

    $('.back-to-top').on('click', function(e) {
        e.preventDefault();

        $('body, html').animate({scrollTop: 0}, 300, function() { $(window).trigger('scroll'); });
    });

}(jQuery,window));

/*if ($('#nav-button-recruitment').length )
{
	$('#nav-button-recruitment').on('click',function(e) {
		e.preventDefault();
		$('#nav-secondary').slideToggle();
	});
}*/


// Subnav highlighting and scrolling

/*if ( $('body').hasClass('recruitment-page') || $('body').hasClass('page-recruitment-page') || $('body').hasClass('page-recruitment') ) {
	
	$('#nav-secondary li:first-child a').addClass('active');
	subnavSmoothScroll();
	
	$(window).on('scroll', function(){
		subnavHighlighting();
	});
} */

function subnavSmoothScroll()  {
	var size = $(window).width();
	if ( size < 768) 
	{
		var topOffset = 75;
	}
	else 
	{
		var topOffset = 128;
	}
	
	$( window ).resize(function() {
		var size = $(window).width();
	});
	
		
/*	$('#nav-secondary a').click(function(e) {
		e.preventDefault();
		var target = this.hash;
			$target = $(target);
		
		if ( size < 768 ) {
			$('html,body').stop().animate({'scrollTop': $target.offset().top-35 - topOffset + 1}, 900, 'swing');
			$('#nav-secondary').slideToggle();
		}
		else {
			$('html,body').stop().animate({'scrollTop': $target.offset().top-35 - topOffset + 1}, 900, 'swing');
		}
	}); */
}

function subnavHighlighting() {
	var size = $(window).width();
	var sectionId = '';

	if ( size < 768) 
	{
		var topOffset = 75;
	}
	else 
	{
		var topOffset = 128;
	}
	
	$( window ).resize(function() {
		var size = $(window).width();
	});
	
	$('section').each( function() {
		var thisTop = ($(window).scrollTop() - $(this).offset().top+70) * -1;
		var thisBottom = (($(window).scrollTop() - $(this).offset().top) * -1) + $(this).outerHeight();
		if (thisTop <= topOffset && thisBottom >= topOffset) {
			sectionId = $(this).attr('id');
		}
	});
	
	$('#nav-secondary a').each(function() {
		var navId = $(this).attr('href');
		navId = navId.replace("#", "");
		if ( navId === sectionId ) {
			$(this).addClass('active');
		} else {
			$(this).removeClass('active');
		}
	});

}

if ( $('body').hasClass('page-video-process') )
{
	$('.video-process .links a, #process-explained .links a,.case-study-cta a').click(function(e){
		e.preventDefault();
		var target = $(this).data('target');
		$(this).parent().hide();
		$(target).fadeIn('slow');
	});
}

if ( $('body').hasClass('page-case-study') )
{
$('.case-study-cta a[href*="request-quote"]').click(function(e){
		e.preventDefault();
		var target = $(this).data('target');
		$(this).hide();
		$(target).fadeIn('slow');
	});
}
$('#free-templates form').submit(function(ev) {
    // Prevent the form from actually submitting
    ev.preventDefault();
	
    // Get the post data
    var data = $(this).serialize();
	$('#free-templates').addClass('submit-show');
    // Send it to the server
    $.post('/', data, function(response) {
        if (response.success) {
	        $('#free-templates .section-heading span').text('Congrats!');
	        $('#free-templates form').css('opacity','0');
	        $('#free-templates .success').fadeIn();
        }
        else
		{
			$('#free-templates').removeClass('submit-show');
	        // Remove previous error messages
	        $('.errors, .error-message').remove();
	        // This is where I want to target the individual fields but I am not having luck figuring it out
	        
		    $('#free-templates form').append('<div class="error-message">Looks like you forgot something. Please fill out all fields.</div>');
		    $('#free-templates .section-heading span').text('Oops!');
	        }
    });
});

$('#request-a-quote form').submit(function(e) {
    // Prevent the form from actually submitting
    e.preventDefault();
	
    // Get the post data
    var data = $(this).serialize();
	$('#request-a-quote').addClass('submit-show');
    // Send it to the server
    $.post('/', data, function(response) {
        if (response.success) {
	        $('#request-a-quote .section-heading span').text('Thank You');
	        $('#request-a-quote form').css('opacity','0');
	        $('#request-a-quote .success').fadeIn();
        }
        else
		{
			$('#request-a-quote').removeClass('submit-show');
	        // Remove previous error messages
	        $('.errors, .error-message').remove();
	        // This is where I want to target the individual fields but I am not having luck figuring it out
	        
	        
		    $('#request-a-quote form').append('<div class="error-message">Looks like you forgot something. Please fill out all fields.</div>');
		    $('#request-a-quote .section-heading span').text('Oops!');
	        }
    });
});

$('#free-templates-2 form').submit(function(ev) {
    // Prevent the form from actually submitting
    ev.preventDefault();
	
    // Get the post data
    var data = $(this).serialize();
	$('#free-templates-2').addClass('submit-show');
    // Send it to the server
    $.post('/', data, function(response) {
        if (response.success) {
	        $('#free-templates-2 .section-heading span').text('Congrats!');
	        $('#free-templates-2 form').css('opacity','0');
	        $('#free-templates-2 .success').fadeIn();
        }
        else
		{
			$('#free-templates-2').removeClass('submit-show');
	        // Remove previous error messages
	        $('.errors, .error-message').remove();
	        // This is where I want to target the individual fields but I am not having luck figuring it out
	        
		    $('#free-templates-2 form').append('<div class="error-message">Looks like you forgot something. Please fill out all fields.</div>');
		    $('#free-templates-2 .section-heading span').text('Oops!');
	        }
    });
});