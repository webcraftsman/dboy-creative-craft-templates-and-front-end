module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				options: {
					style: 'compressed'
				},
				files: {
					'_css/dboy.css' : '_source/_sass/dboy.scss'
				}
			}
		},
		concat: {
			dist: {
				src: ['_source/_js/*.js'],
				dest: '_js/main.js'
			}
		},
		uglify: {
			build: {
				src: '_js/main.js',
				dest: '_js/main.min.js'
			}
		},
		legacssy: {
			dist: {
				options: {
					legacyWidth: 980
				},
				files: {
					'_css/dboy-legacy.css': '_css/dboy.css'
				}
			}
		},
		watch: {
				css: {
					files: '**/*.scss',
					tasks: ['sass','legacssy']
				},
				scripts: {
					files:'_source/_js/*.js',
					tasks:['concat','uglify']
				}
		},
	});
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-legacssy');

	grunt.registerTask('default',['concat','uglify']);
	grunt.registerTask('prod',['s3','cdn']);
  grunt.registerTask('imageoptim', ['imageoptim']);
}
